﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFire : MonoBehaviour
{
    [SerializeField] private GameObject bullet, gun;
    [SerializeField] private Camera camera;
    void Update()
    {
        
        Ray ray = camera.ScreenPointToRay(Input.mousePosition); 
        Debug.DrawRay(ray.origin, ray.direction * 10, Color.yellow);
        if (Input.GetMouseButtonDown(0))
        {
            var myBullet = Instantiate(bullet, gun.transform.position, gun.transform.rotation);
            myBullet.GetComponent<BulletMove>().SetMyDirection(ray.direction);
        }
    }
}

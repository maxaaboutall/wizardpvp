﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMove : MonoBehaviour
{
    [SerializeField] 
    private float speed = 15f;
    
    private Vector3 myDirection;
    void Update()
    {
        
    }

    private void Start()
    {
        var myRb = GetComponent<Rigidbody>();
        myRb.AddForce(myDirection * speed * Time.deltaTime, ForceMode.VelocityChange);
        StartCoroutine(die());
    }

    private IEnumerator die()
    {
        yield return new WaitForSeconds(2);
        Destroy(gameObject);
    }

    public void SetMyDirection(Vector3 direction)
    {
        myDirection = direction;
    }
    
    private void OnCollisionEnter(Collision other)
    {
        Debug.Log("In Collision " + other.gameObject.name);
        if(other.gameObject.name != "Gun" && other.gameObject.name != "Capsule" && other.gameObject.name != "Player")
        {
            //Debug.Log("In first if");
            if (gameObject.GetComponentInChildren<ParticleSystem>() != null)
            {
                //Debug.Log("In second if");
                //Destroy(gameObject.GetComponentInChildren<ParticleSystem>());
            }
            Destroy(gameObject);
        }
    }
}
